pt-BR:

O emulador de jogos Phoenix foi criado por Grishin Maxim Leonidovich (Altmer) e a equipe do projeto do emulador Phoenix e emula os jogos do 3DO, Jaguar da Atari, Coleco Vision, SG-1000, Master System e Game Gear.

Contém: o arquivo ou pacote de instalação .deb e um arquivo de texto em idioma "pt-BR" explicando como utilizá-lo.

Transfira ou baixe ou descarregue os arquivos "phoenix-emu_2.8.JAG-1_amd64.deb", "phoenix-emu_2.8.JAG-1_amd64.deb.md5.sum", "phoenix-emu_2.8.JAG-1_amd64.deb.sha256.sum" e o arquivo de texto ".txt".

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Das Phoenix Emulator Game wurde von Grishin Maxim Leonidovich (Altmer) und dem Phoenix Emulator Project Team erstellt und emuliert Spiele von 3DO, Jaguar von Atari, Coleco Vision, SG-1000, Master System und Game Gear.

Es enthält: die .deb-Installationsdatei oder das Paket und eine Textdatei in "pt-BR"-Sprache, die erklärt, wie man es benutzt.

Laden Sie die Dateien "phoenix-emu_2.8.JAG-1_amd64.deb", "phoenix-emu_2.8.JAG-1_amd64.deb.md5.sum", "phoenix-emu_2.8.JAG-1_amd64.deb.sha256.sum" und die Textdatei ".txt" herunter oder laden Sie sie herunter oder laden Sie sie herunter.

Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

fr :

Le jeu Phoenix Emulator a été créé par Grishin Maxim Leonidovich (Altmer) et l'équipe du projet Phoenix Emulator et émule les jeux 3DO, Atari Jaguar, Coleco Vision, SG-1000, Master System et Game Gear.

Contient : le fichier ou package d'installation .deb et un fichier texte en "pt-BR" expliquant comment l'utiliser.

Téléchargez les fichiers "phoenix-emu_2.8.JAG-1_amd64.deb", "phoenix-emu_2.8.JAG-1_amd64.deb.md5.sum", "phoenix-emu_2.8.JAG-1_amd64.deb.sha256.sum" et le fichier texte ".txt".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

Il Phoenix Emulator Game è stato creato da Grishin Maxim Leonidovich (Altmer) e dal Phoenix Emulator Project Team ed emula i giochi 3DO, Atari Jaguar, Coleco Vision, SG-1000, Master System e Game Gear.

Contiene: il file o pacchetto di installazione .deb e un file di testo in lingua "pt-BR" che spiega come utilizzarlo.

Scarica i file "phoenix-emu_2.8.JAG-1_amd64.deb", "phoenix-emu_2.8.JAG-1_amd64.deb.md5.sum", "phoenix-emu_2.8.JAG-1_amd64.deb.sha256.sum" e il file di testo ".txt".

Tutti i crediti e i diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe

- - - - -

es:

El Phoenix Emulator Game fue creado por Grishin Maxim Leonidovich (Altmer) y el Phoenix Emulator Project Team y emula los juegos 3DO, Atari Jaguar, Coleco Vision, SG-1000, Master System y Game Gear.

Contiene: el archivo o paquete de instalación .deb y un archivo de texto en idioma "pt-BR" que explica cómo usarlo.

Descargue los archivos "phoenix-emu_2.8.JAG-1_amd64.deb", "phoenix-emu_2.8.JAG-1_amd64.deb.md5.sum", "phoenix-emu_2.8.JAG-1_amd64.deb.sha256.sum" y el archivo de texto ".txt".

Todos los créditos y derechos están incluidos en los archivos, en relación con el trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

en:

The Phoenix Emulator Game was created by Grishin Maxim Leonidovich (Altmer) and the Phoenix Emulator Project Team and emulates games from 3DO, Jaguar by Atari, Coleco Vision, SG-1000, Master System and Game Gear.

Contains: the .deb installation file or package and a text file in "pt-BR" language explaining how to use it.

Download the files "phoenix-emu_2.8.JAG-1_amd64.deb", "phoenix-emu_2.8.JAG-1_amd64.deb.md5.sum", "phoenix-emu_2.8.JAG-1_amd64.deb.sha256.sum" and the text file ".txt".

All credits and rights are included in the files, in respect of the volunteer work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe
